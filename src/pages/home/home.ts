import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as midi from 'midi-synth';
import { MidiAudio } from '../../providers/midi-audio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  options : any = [];
  selectedIndex : any;
  
  constructor(public navCtrl: NavController, public midi: MidiAudio) {
    this.Init();
    
  }

  loadMidiUrl() {
    this.midi.initValues.loadMIDIUrl('ws.mid');
  }
  loadMidi(files) {
    var reader = new FileReader();
    reader.onload = (e) => {
      this.midi.initValues.loadMIDI(reader.result);
    }
    reader.readAsArrayBuffer(files[0]);
  }
  playMidi() {
    this.midi.initValues.playMIDI();
  }
  stopMidi() {
    this.midi.initValues.stopMIDI();
  }
  SetProgram(p) {
    console.log(this.selectedIndex);
    this.midi.initValues.send(0xc0, p);
  }
  Init() {
    //this.midi.initValues=new WebAudioTinySynth({voices:64});
    for (let i = 0; i < 128; ++i) {
      let o = document.createElement("option"); 
      this.options.push(this.midi.initValues.getTimbreName(0, i));
      // document.getElementById('prog').appendChild(o);
    }
    let thi = this;
    setInterval(function () {
      var st = thi.midi.initValues.getPlayStatus();
      document.getElementById("status").innerHTML = "Play:" + st.play + "  Pos:" + st.curTick + "/" + st.maxTick;
    }, 100);
  }
  Test() {
    var o = this.midi.initValues.getAudioContext().createOscillator();
    o.connect(this.midi.initValues.getAudioContext().destination);
    o.start(0);
    o.stop(this.midi.initValues.getAudioContext().currentTime + 1);
    console.log(this.midi.initValues)
  }

  send(){
    this.midi.initValues.send([0x80,60,0]);
  }

}
